class Tag < ActiveRecord::Base
	validates :tag, presence: true, uniqueness: true

	has_many :taggeds
	has_many :messages, through: :taggeds

	def self.scan message
		content = message.content
		index = 0
		hashtags = content.scan(/(?:\s|^)(?:#(?!(?:\d+|\w+?_|_\w+?)(?:\s|$)))(\w+)(?=\s|$)/i).map{|x| x[0]}
		hashtags.each do|hashtag|
			tag = Tag.find_or_create_by(tag: hashtag)
			tag.save

			tagged = Tagged.new(tag_id: tag.id, message_id: message.id)
			tagged.save
		end
	end
end
