class Tagged < ActiveRecord::Base
	validates :tag_id, presence: true
	validates :message_id, presence: true

	belongs_to :tag
	belongs_to :message
end
