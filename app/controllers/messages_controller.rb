class MessagesController < ApplicationController
	before_action :new_message, only: [:index, :tags, :followed]

	def index
		@messages = Message.all.order("id DESC")
	end

	def create
		@message = current_user.messages.build(params.require(:message).permit(:content))
		if @message.save
			Tag.scan(@message)
			respond_to do |format|
				format.html { redirect_to messages_path }
			end
		else
			# didnt work tell user
			flash[:notice] = "Failed"
			redirect_to messages_path
		end
	end

	def tags
		tagStrings = params[:tags][:tags]
		puts tagStrings.inspect
		@messages = tags_messages tagStrings
	end

	def followed
		@messages = followed_messages
		puts current_user.following.inspect
	end

	def destroy
		@message = Message.find(params[:id])
		@message.destroy
		redirect_to messages_path
	end

	def refresh
		page = request.headers["Referer"].split('/')[-1]
		case page
		when "followed"
			@messages = followed_messages
		when "tags"
			@messages = tags_messages params[:tags].gsub('+', ' ')
		else
			@messages = Message.all
		end
		last_id = params[:last_id]
		if @messages.instance_of? Array
			@messages = @messages.sort! { |a,b| b.id <=> a.id }
			@messages.delete_if do |message|
				if message.id <= last_id.to_i
					true
				end
			end
		else
			@messages = @messages.where("id > ?", last_id).order("id DESC")
		end
		respond_to do |format|
			format.js
		end
	end

	private

		def new_message
			@message = Message.new
		end

		def followed_messages
			messagesArray = []
			followings = current_user.following
			followings.each do |following|
				messagesArray << following.user.messages
			end
			if messagesArray.count > 0
				@messages = messagesArray[0]
				(1..(messagesArray.count-1)).each do |i|
					@messages = (@messages + messagesArray[i])
				end
				if messagesArray.count > 1
					@messages = @messages.sort! { |a,b| b.id <=> a.id }
				else
					@messages = @messages.order("id DESC")
				end
			else
				@messages = nil
			end
			@messages
		end

		def tags_messages tagStrings
			
			messagesArray = []
			puts "tagStrings: "+ tagStrings.inspect
			tagStrings.split(' ').each do |tagString|
				tag = Tag.find_or_create_by(tag: tagString)
				if !tag.nil?
					@messages = tag.messages
					messagesArray << @messages
				end
			end

			if messagesArray.count > 0
				@messages = messagesArray[0]
				(1..(messagesArray.count-1)).each do |i|
					@messages = (@messages & messagesArray[i])
				end
				if messagesArray.count > 1
					@messages = @messages.sort! { |a,b| b.id <=> a.id }
				else	
					@messages = @messages.order("id DESC")
				end
			else
				@messages = nil
			end
			@messages
		end
end
