class FollowingsController < ApplicationController
	def follow
		user = params[:user]
		followings = Followings.new(user_id: user, follower_id: current_user.id)
		if followings.save
			redirect_to followed_path
		else
			flash[:notice] = "Failed"
			redirect_to root_path
		end
	end

	def unfollow
		user = params[:user]
		followings = Followings.find_by(user_id: user, follower_id: current_user.id)
		followings.destroy
		redirect_to followed_path
	end
end
