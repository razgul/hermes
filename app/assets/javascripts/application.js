// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require jquery
//= require bootstrap-sprockets

$(document).ready(function () {
	//refreshPartial()
    // will call refreshPartial every 30 seconds
    setInterval(refreshPartial, 30000)

});
if (typeof tags == 'undefined'){
	var tags = '';
}
// calls action refreshing the partial
function refreshPartial() {
	$('.newMessage').removeClass('newMessage');
  $.ajax({
  	type: "GET",
    url: "messages/refresh",
    data: {"last_id": last_id,
			"tags": tags}
 })
}