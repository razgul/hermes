require 'test_helper'

class TagTest < ActiveSupport::TestCase
	test "should not save tag without tag string" do
		tag = Tag.new
		assert_not tag.save
	end

	test "should save tag with tag string" do
		tag = Tag.new(tag: "#tag")
		assert tag.save
	end
  # test "the truth" do
  #   assert true
  # end
end
