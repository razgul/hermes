require 'test_helper'

class TaggedTest < ActiveSupport::TestCase
	test "should not save tagged without ids" do
		tagged = Tagged.new
		assert_not tagged.save
	end

	test "should save tagged for existing ids" do
		tagged = Tagged.new(tag_id: 1, message_id: 1)
		assert_not tagged.save
	end
  # test "the truth" do
  #   assert true
  # end
end
